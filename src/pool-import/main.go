//
//  pool-import
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of pool-import.
//
//  pool-import is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  pool-import is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with pool-import. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"code.helsinki.at/rhrd-go/rddb"
	"code.helsinki.at/rhrd-go/rhimport"
)

const (
	RD_CONF = "/etc/rd.conf"
	RD_USER = "pool-import"
)

func Done(res rhimport.Result, userdata interface{}) bool {
	c := userdata.(chan<- rhimport.Result)
	c <- res
	return true
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Usage: pool-import <pool group>")
	}
	group := os.Args[1]

	conf, err := rhimport.NewConfig(RD_CONF, "http://localhost/rd-bin/rdxport.cgi", "/tmp/", "snd/")
	if err != nil {
		log.Fatal("Error parsing configuration file:", err)
	}

	db, err := rddb.NewDB(RD_CONF)
	if err != nil {
		log.Fatal("Error initializing Rivdenll DB:", err)
	}
	defer db.Cleanup()

	// var password string
	// if password, err = db.GetInterface().GetPassword(RD_USER, false); err != nil {
	// 	log.Fatalf("Error fetching password for user '%s': %v", RD_USER, err)
	// }
	// if err := rhimport.RemoveCart(cart, "https://import.helsinki.at/rd-bin/rdxport.cgi", RD_USER, password); err != nil {
	// 	log.Fatalf("Error deleting cart %d: %v", cart.Number, err)
	// }

	stdlog := log.New(os.Stderr, "[std] ", log.LstdFlags)
	//dbglog := log.New(os.Stderr, "[dbg] ", log.LstdFlags)
	var dbglog *log.Logger
	sessions, err := rhimport.NewSessionStore(conf, db.GetInterface(), stdlog, dbglog)
	if err != nil {
		log.Fatal("Error initializing Session Store:", err)
	}
	defer sessions.Cleanup()

	if ok, err := db.GetInterface().CheckMusicGroup(group); err != nil {
		stdlog.Fatal("Error checking Pool Group:", err)
	} else if !ok {
		stdlog.Fatalf("Error '%s' is not a pool group", group)
	}

	C := make(chan os.Signal, 1)
	signal.Notify(C, os.Interrupt, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM)

	stdlog.Println("***************************************************************")
	stdlog.Printf("*** will import into group '%s'", group)
	stdlog.Println("***************************************************************")

	go func() {
		defer func() {
			C <- syscall.SIGTERM
		}()

		carts, err := db.GetInterface().GetPoolCartList(rddb.PoolListEntry{Group: group})
		if err != nil {
			stdlog.Fatal("Error fetching Pool Carts:", err)
		}

		stdlog.Printf("got %d carts from pool:", len(carts))
		for _, cart := range carts {
			stdlog.Printf(" %d: '%s' | %s / %s / %s)", cart.Number, cart.UserDefined, cart.Artist, cart.Album, cart.Title)
		}

		files, err := callFileHasher(group)
		if err != nil {
			stdlog.Fatal("Calling file hasher failed:", err)
		}

		stdlog.Println("")
		stdlog.Printf("  %d files", len(files))
		stdlog.Println("****************************")
		for hash, file := range files {
			stdlog.Printf("%s: %s (%d bytes)", hash, file.Path, file.Size)
		}
		stdlog.Println("****************************")

	}()

	<-C
}
