//
//  pool-import
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of pool-import.
//
//  pool-import is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  pool-import is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with pool-import. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"bytes"
	"encoding/json"
	"os"
	"os/exec"
	"path/filepath"
	"time"
)

type File struct {
	Path string
	Size int64
}

type FileMap map[string]File

func openGroupConfig(group string) (*os.File, error) {
	cwd, err := filepath.Abs(".")
	if err != nil {
		return nil, err
	}
	fileName := filepath.Join(cwd, filepath.Clean("/"+group+"/config.json"))
	return os.Open(fileName)
}

func createLogFile(group string) (log *os.File, err error) {
	cwd, err := filepath.Abs(".")
	if err != nil {
		return nil, err
	}
	fileName := filepath.Join(cwd, filepath.Clean("/"+group), "logs", time.Now().Format(time.RFC3339)+".json")
	return os.OpenFile(fileName, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0640)
}

func callFileHasher(group string) (files FileMap, err error) {
	var stdin, log *os.File
	if stdin, err = openGroupConfig(group); err != nil {
		return
	}
	if log, err = createLogFile(group); err != nil {
		return
	}
	var stdout bytes.Buffer

	// TODO: actually call this via ssh
	cmd := exec.Command("bin/file-hasher")
	cmd.Stdin = stdin
	cmd.Stdout = &stdout
	cmd.Stderr = log

	if err = cmd.Run(); err != nil {
		return
	}

	if err = json.NewDecoder(&stdout).Decode(&files); err != nil {
		return
	}

	return
}
