//
//  pool-import
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of pool-import.
//
//  pool-import is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  pool-import is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with pool-import. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"log"
	"os"
	"path/filepath"
)

type Dir struct {
	root   string
	stdlog *log.Logger
}

func handleEntry(C chan<- string, path string, info os.FileInfo, err error, stdlog *log.Logger) error {
	if err != nil {
		return err
	}
	if info.IsDir() {
		stdlog.Printf("entering directory: %s", path)
		return nil
	}
	if !info.Mode().IsRegular() {
		stdlog.Printf(" - skipping (special file):  %s", info.Name())
		return nil
	}
	if checkFileExt(path) {
		if absPath, err := filepath.Abs(path); err != nil {
			stdlog.Printf(" - skipping (%v):  %s", err, info.Name())
		} else {
			C <- absPath
		}
	} else {
		stdlog.Printf(" - skipping (unknown extension):  %s", info.Name())
	}
	return nil
}

func (d *Dir) Walk(C chan<- string) (err error) {
	defer close(C)

	return filepath.Walk(d.root, func(path string, info os.FileInfo, err error) error {
		return handleEntry(C, path, info, err, d.stdlog)
	})
}

func NewDir(root string, stdlog *log.Logger) (dir *Dir) {
	dir = &Dir{root: root, stdlog: stdlog}
	return
}
