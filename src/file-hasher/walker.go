//
//  pool-import
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of pool-import.
//
//  pool-import is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  pool-import is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with pool-import. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"path/filepath"
	"strings"
)

var validExtensions []string

func checkFileExt(path string) bool {
	if len(validExtensions) == 0 {
		return true
	}

	ext := strings.ToLower(filepath.Ext(path))
	for _, e := range validExtensions {
		if ext == "."+e {
			return true
		}
	}
	return false
}

type Walker interface {
	Walk(C chan<- string) error
}
