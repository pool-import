//
//  pool-import
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of pool-import.
//
//  pool-import is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  pool-import is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with pool-import. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

type Request struct {
	NumThreads uint   `json:"threads"`
	HashAlgo   string `json:"algo"`
	Sources    struct {
		Directories  []string `json:"dir"`
		M3UPlaylists []string `json:"m3u"`
	} `json:"src"`
	ValidExtensions []string `json:"valid-ext"`
}

func main() {
	stdlog := log.New(os.Stderr, "[std] ", log.LstdFlags)

	var req Request
	if err := json.NewDecoder(os.Stdin).Decode(&req); err != nil {
		stdlog.Fatalf("Error decoding request: %v", err)
	}

	validExtensions = req.ValidExtensions
	for i, ext := range validExtensions {
		validExtensions[i] = strings.ToLower(ext)
	}

	C := make(chan os.Signal, 1)
	signal.Notify(C, os.Interrupt, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM)

	starttime := time.Now()
	go func() {
		defer func() {
			C <- syscall.SIGTERM
		}()
		stdlog.Println("***************************************************************")
		h := NewHasher(req.HashAlgo, req.NumThreads, stdlog)
		if h == nil {
			os.Exit(1)
		}
		stdlog.Println("")
		stdlog.Println("******************************")

		for _, root := range req.Sources.Directories {
			src := NewDir(root, stdlog)
			stdlog.Printf("*** hashing all the files in '%s'", root)
			stdlog.Println("")
			if err := h.ComputeHashes(src); err != nil {
				stdlog.Println("")
				stdlog.Fatalf("ERROR: %v", err)
			}
			stdlog.Println("")
			stdlog.Println("******************************")
		}
		for _, list := range req.Sources.M3UPlaylists {
			src := NewM3u(list, stdlog)
			stdlog.Printf("*** hashing all the files from '%s'", list)
			stdlog.Println("")
			if err := h.ComputeHashes(src); err != nil {
				stdlog.Println("")
				stdlog.Fatalf("ERROR: %v", err)
			}
			stdlog.Println("")
			stdlog.Println("******************************")
		}

		stdlog.Println("")
		stdlog.Println("***************************************************************")
		stdlog.Printf("***  hashed %d files in %v", len(h.Files), time.Since(starttime))
		stdlog.Println("***************************************************************")

		enc := json.NewEncoder(os.Stdout)
		if err := enc.Encode(h.Files); err != nil {
			stdlog.Fatalf("Error encoding Result: %v", err)
		}
	}()

	<-C
}
