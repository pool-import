//
//  pool-import
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of pool-import.
//
//  pool-import is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  pool-import is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with pool-import. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"log"
	"os"
	"path/filepath"

	"github.com/ushis/m3u"
)

type M3u struct {
	filename string
	stdlog   *log.Logger
}

func (m *M3u) Walk(C chan<- string) (err error) {
	defer close(C)

	m.stdlog.Printf("loading m3u-playlist: %s", m.filename)

	var file *os.File
	if file, err = os.Open(m.filename); err != nil {
		return
	}
	defer file.Close()

	var old_wd string
	if old_wd, err = os.Getwd(); err != nil {
		return
	}
	defer func(wd string) {
		if err := os.Chdir(wd); err != nil {
			m.stdlog.Printf("ERROR: changing back to original working directory: %v", err)
		}
	}(old_wd)

	if err = os.Chdir(filepath.Dir(m.filename)); err != nil {
		m.stdlog.Printf("ERROR: changing to m3u directory: %v", err)
	}

	var playlist m3u.Playlist
	if playlist, err = m3u.Parse(file); err != nil {
		return
	}

	for _, track := range playlist {
		if checkFileExt(track.Path) {
			if absPath, err := filepath.Abs(track.Path); err != nil {
				m.stdlog.Printf(" - skipping (%v):  %s", err, filepath.Base(track.Path))
			} else {
				C <- absPath
			}
		} else {
			m.stdlog.Printf(" - skipping (unknown extension):  %s", filepath.Base(track.Path))
		}
	}
	return nil
}

func NewM3u(path string, stdlog *log.Logger) (m *M3u) {
	m = &M3u{filename: path, stdlog: stdlog}
	return
}
