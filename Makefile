##
##  pool-import
##
##  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
##
##  This file is part of pool-import.
##
##  pool-import is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  pool-import is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with pool-import. If not, see <http://www.gnu.org/licenses/>.
##

curdir:= $(shell pwd)
ifndef GOROOT
GOCMD := GOPATH=$(curdir) go
else
GOCMD := GOPATH=$(curdir) $(GOROOT)/bin/go
endif

EXECUTEABLE := pool-import
EXECUTEABLE_HASHER := file-hasher

LIBS := "code.helsinki.at/rhrd-go/rhimport" \
        "code.helsinki.at/rhrd-go/rddb" \
        "github.com/ushis/m3u" \
        "golang.org/x/crypto/blake2b"



.PHONY: getlibs updatelibs vet format build clean distclean
all: build


getlibs:
	@$(foreach lib,$(LIBS), echo "fetching lib: $(lib)"; $(GOCMD) get $(lib);)

updatelibs:
	@$(foreach lib,$(LIBS), echo "updating lib: $(lib)"; $(GOCMD) get -u $(lib);)

vet:
	@echo "vetting: $(EXECUTEABLE)"
	@$(GOCMD) vet $(EXECUTEABLE)
	@echo "vetting: $(EXECUTEABLE_HASHER)"
	@$(GOCMD) vet $(EXECUTEABLE_HASHER)

format:
	@echo "formating: $(EXECUTEABLE)"
	@$(GOCMD) fmt $(EXECUTEABLE)
	@echo "formating: $(EXECUTEABLE_HASHER)"
	@$(GOCMD) format $(EXECUTEABLE_HASHER)

build: getlibs
	@echo "installing: $(EXECUTEABLE)"
	@$(GOCMD) install $(EXECUTEABLE)
	@echo "installing: $(EXECUTEABLE_HASHER)"
	@$(GOCMD) install $(EXECUTEABLE_HASHER)

clean:
	rm -rf pkg/*/$(EXECUTEABLE)
	rm -rf pkg/*/$(EXECUTEABLE_HASHER)
	rm -rf bin

distclean: clean
	@$(foreach dir,$(shell ls src/),$(if $(and $(subst $(EXECUTEABLE),,$(dir)), $(subst $(EXECUTEABLE_HASHER),,$(dir))),$(shell rm -rf src/$(dir))))
	rm -rf pkg
